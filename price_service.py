import datetime
import logging
from datetime import date
import pymongo
from pymongo import MongoClient
from flask import Flask, request
from flask_restplus import Resource, Api, fields
import pandas_datareader as pdr

# Connect to mongo db collection
client = MongoClient('localhost', 27017)
db = client.mydatabase
mongo_tickers = db.RequestedTickers

LOG = logging.getLogger(__name__)
app = Flask(__name__)
api = Api(app)

# Model factory: instantiate and register models to your api
stock = api.model('stock', {
    'ticker': fields.String(required=True, description='Ticker'),
})

tickers = api.model('tickers', {
    'tickers': fields.List(required=True, description='Tickers', cls_or_instance=fields.String())
})


def log(df):
    LOG.error('Recieved price data:')
    LOG.error(str(df))


@api.route('/v1/price/<ticker>')
@api.param('ticker', 'The stock ticker to get a price for')
class Price(Resource):

    @api.expect(tickers)
    def post(self):

        start_date = datetime.datetime.now() - datetime.timedelta(1)

        outputs = {'stocks': []}

        for ticker in api.payload['tickers']:
            try:
                df = pdr.get_data_yahoo(ticker, start_date)[['Close']]
            except Exception as ex:
                LOG.error('Error getting data: ' + str(ex))

            LOG.error('Recieved price data:')
            LOG.error(str(df))

            df['Date'] = df.index

            outputs['stocks'].append(
                {'ticker': ticker,
                 'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
                 'price': df.iloc[-1]['Close']}
            )

        return outputs

    def get(self, ticker):

        start_date = datetime.datetime.now() - datetime.timedelta(1)

        # Convert input ticker into list
        result = [x.strip() for x in ticker.split(',')]
        LOG.error(result)
        # Then create output list
        output = {}
        # For every ticker in list, get dataframe and info. Then add that to the output list.
        for one_ticker in result:

            df = pdr.get_data_yahoo(one_ticker, start_date)[['Close']]

            df['Date'] = df.index
            output[one_ticker] = {'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
                                  'price': df.iloc[-1]['Close']}
            mongo_tickers.insert_one(
                {'Ticker': ticker, 'Time': datetime.datetime.now()})
        return output


@api.route('/v1/price/<ticker>/<start_date>')
@api.param('ticker', 'The stock ticker to get a price from a start date')
class PriceFrom(Resource):
    def get(self, ticker, start_date):
        df = pdr.get_data_yahoo(ticker, start_date)[['Close']]
        mongo_tickers.insert_one(
            {'Ticker': ticker, 'Time': datetime.datetime.now()})
        log(df)
        return df.to_dict()


@api.route('/v1/price/<ticker>/<start_date>/<end_date>')
@api.param('ticker', 'The stock ticker to get a price from a start date')
class PriceBetween(Resource):
    def get(self, ticker, start_date, end_date):
        df = pdr.get_data_yahoo(ticker, start_date, end_date)[['Close']]
        log(df)
        mongo_tickers.insert_one(
            {'Ticker': ticker, 'Time': datetime.datetime.now()})
        return df.to_dict()


@api.route('/v1/info/<ticker>/')
@api.param('ticker', 'The stock ticker to get more information of')
class PriceInfo(Resource):
    def get(self, ticker):
        mongo_tickers.insert_one(
            {'Ticker': ticker, 'Time': datetime.datetime.now()})
        df = pdr.get_quote_yahoo(ticker)
        log(df)
        return df.to_dict()


@api.route('/v1/ma/<ticker>/<int:num_days>')
@api.param('ticker', 'The stock ticker to get a price from a start date')
class MovingAverage(Resource):
    def get(self, ticker, num_days):
        df = pdr.get_data_yahoo(ticker)[['Close']]
        log(df)
        mongo_tickers.insert_one(
            {'Ticker': ticker, 'Time': datetime.datetime.now()})
        df = df.rolling(window=num_days)['Close'].mean()[date.today()]

        return df


if __name__ == '__main__':
    app.run(debug=True)
