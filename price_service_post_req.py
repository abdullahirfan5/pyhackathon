###### ALTERNATIVE SOLUTION #########

############ This script uses a POST request to send a list of tickers and their associated date ranges ############


import datetime
import logging

from flask import Flask, request
from flask_restplus import Resource, Api, fields
import pandas_datareader as pdr

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)

tick = api.model('tick', {
    'ticker': fields.String(required=True, description='Ticker'),
    'start_date': fields.DateTime(required=False, description='Date'),
    'end_date': fields.DateTime(required=False, description='Date')
})

tickers = api.model('tickers', {
    'tickers': fields.List(fields.Nested(tick), required=True, description='Tickers')
    })


stock = api.model('stock', {
    'ticker': fields.String(required=True, description='Ticker'),
    })

@api.route('/v1/price')
class Ticker(Resource):
    @api.expect(tickers)
    def post(self):

        outputs = {'stocks': []}


        for ticker in api.payload['tickers']:

            start_date = ticker['start_date']
            end_date = ticker['end_date']
            tick = ticker['ticker']
            
            try:
                df = pdr.get_data_yahoo(tick, start_date, end_date)[['Close']]
            except Exception as ex:
                LOG.error('Error getting data: ' + str(ex))
            
            LOG.error('Recieved price data:')
            LOG.error(str(df))

            df['Date'] = df.index

            info = []
            for i, d in df.iterrows():
                print('HERE')
                print(d)
                info.append(
                    {'date': d['Date'].strftime("%Y-%m-%d"),
                     'price': d['Close']}
                )

            # outputs['stocks'].append(df.to_dict())

            outputs['stocks'].append(
                        {'ticker': tick,
                        'prices': info}
            )
        return outputs

@api.route('/v1/stock')
class Stock(Resource):

    @api.expect(stock)
    def post(self):
        LOG.error('Received payload:')
        LOG.error(api.payload)

        # store this record in a database

        return {'result': 'success',
                'ticker': api.payload['ticker']}


if __name__ == '__main__':
    app.run(debug=True)
